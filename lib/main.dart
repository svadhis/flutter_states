import 'package:flutter/material.dart';
import 'package:flutter_states/getx/injector/getbuilder_injector.dart';
import 'package:flutter_states/getx/injector/getx_injector.dart';
import 'package:flutter_states/getx/injector/obx_injector.dart';
import 'package:flutter_states/provider/injector/provider_injector.dart';
import 'package:flutter_states/redux/injector/redux_injector.dart';
import 'package:flutter_states/scoped_model/injector/scoped_model_injector.dart';
import 'package:flutter_states/stateful_widget/component/stateful_widget_component.dart';
import 'package:flutter_states/states_rebuilder/component/states_rebuilder_component_new.dart';
import 'package:flutter_states/states_rebuilder/injector/states_rebuilder_injector.dart';
import 'package:flutter_states/states_rebuilder/injector/states_rebuilder_injector_full.dart';
import 'package:flutter_states/states_rebuilder/injector/states_rebuilder_injector_new.dart';
import 'package:flutter_states/states_rebuilder/state/states_rebuilder_state.dart';
import 'package:get/get.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter States',
      theme: ThemeData(),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter States'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              FlatButton(
                onPressed: () => Get.to(StatefulWidgetComponent()),
                child: Text('Stateful Widget')
              ),
              FlatButton(
                onPressed: () => Get.to(ScopedModelInjector()),
                child: Text('ScopedModel')
              ),
              FlatButton(
                onPressed: () => Get.to(ProviderInjector()),
                child: Text('Provider')
              ),
              FlatButton(
                onPressed: () => Get.to(GetxInjector()),
                child: Text('GetX')
              ),
              FlatButton(
                onPressed: () => Get.to(ObxInjector()),
                child: Text('GetX - Obx')
              ),
              FlatButton(
                onPressed: () => Get.to(GetbuilderInjector()),
                child: Text('GetX - Getbuilder')
              ),
              FlatButton(
                onPressed: () => Get.to(ReduxInjector()),
                child: Text('Redux')
              ),
              FlatButton(
                onPressed: () => Get.to(StatesRebuilderInjector()),
                child: Text('States Rebuilder')
              ),
              FlatButton(
                onPressed: () => Get.to(StatesRebuilderInjectorFull()),
                child: Text('States Rebuilder - Full')
              ),
              FlatButton(
                onPressed: () => Get.to(StatesRebuilderComponentNew()),
                child: Text('States Rebuilder - New')
              ),
              MyWidget()
            ],
          ),
        ),
      ),
    );
  }
}

class MyState {
  int number = 0;

  void increment() {
    number++;
    print(number);
  }
}

final Injected<MyState> myState = RM.inject(() => MyState());

class MyWidget extends StatelessWidget {
  const MyWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        onPressed: () => myState.setState((s) => s.increment()), 
        child: myState.whenRebuilder(
          onIdle: null, 
          onWaiting: null, 
          onData: () => Text('increment number ${myState.state.number}'), 
          onError: null
        )
      )
    );
  }
}