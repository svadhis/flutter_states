import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/states_rebuilder/state/states_rebuilder_state.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class StatesRebuilderComponent extends StatelessWidget {
  const StatesRebuilderComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    ReactiveModel<StatesRebuilderState> model = RM.get<StatesRebuilderState>(context: context);

    return Component(
      name: 'States Rebuilder',
      color: Colors.green,
      counter: model.state.counter,
      duration: model.state.duration,
      callback: () => model.setState((s) => s.increment())
    );
  }
}