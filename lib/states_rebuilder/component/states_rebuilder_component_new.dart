import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/states_rebuilder/injector/states_rebuilder_injector_new.dart';

class StatesRebuilderComponentNew extends StatelessWidget {
  const StatesRebuilderComponentNew({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return srState.rebuilder(() => Component(
      name: 'States Rebuilder - New',
      color: Colors.teal,
      counter: srState.state.counter,
      duration: srState.state.duration,
      callback: () => srState.setState((s) => s.increment())
    ));
      
    
  }
}