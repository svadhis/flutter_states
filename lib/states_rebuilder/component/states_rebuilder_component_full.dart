import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/states_rebuilder/state/states_rebuilder_state.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class StatesRebuilderComponentFull extends StatelessWidget {
  const StatesRebuilderComponentFull({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StateBuilder<StatesRebuilderState>(
      builder: (context, model) {
        return Component(
          name: 'States Rebuilder - Full',
          color: Colors.cyan,
          counter: model.state.counter,
          duration: model.state.duration,
          callback: () => model.setState((s) => s.increment())
        );
      }
    );
  }
}