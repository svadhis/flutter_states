import 'package:flutter/material.dart';
import 'package:flutter_states/states_rebuilder/component/states_rebuilder_component.dart';
import 'package:flutter_states/states_rebuilder/state/states_rebuilder_state.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class StatesRebuilderInjector extends StatelessWidget {
  const StatesRebuilderInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Injector(
      inject: [Inject(() => StatesRebuilderState())],
      builder: (context) => StatesRebuilderComponent(),
    );
  }
}