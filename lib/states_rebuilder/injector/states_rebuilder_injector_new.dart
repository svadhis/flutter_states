import 'package:flutter_states/states_rebuilder/state/states_rebuilder_state.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

final Injected<StatesRebuilderState> srState = RM.inject(() => StatesRebuilderState());