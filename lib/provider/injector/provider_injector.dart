import 'package:flutter/material.dart';
import 'package:flutter_states/provider/component/provider_component.dart';
import 'package:flutter_states/provider/state/provider_state.dart';
import 'package:provider/provider.dart';

class ProviderInjector extends StatelessWidget {
  const ProviderInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ProviderState(),
      child: ProviderComponent()
    );
  }
}