import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/provider/state/provider_state.dart';
import 'package:provider/provider.dart';

class ProviderComponent extends StatelessWidget {
  const ProviderComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProviderState>(
      builder: (context, model, child) {
        return Component(
          name: 'Provider',
          color: Colors.green,
          counter: model.counter,
          duration: model.duration,
          callback: () => model.increment()
        );
      }
    );
  }
}