import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_states/redux/component/redux_component.dart';
import 'package:flutter_states/redux/state/redux_reducer.dart';
import 'package:flutter_states/redux/state/redux_store.dart';
import 'package:redux/redux.dart';

class ReduxInjector extends StatelessWidget {
  const ReduxInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<ReduxStore>(
      store: Store<ReduxStore>(reduxReducer, initialState: ReduxStore()),
      child: ReduxComponent(),
    );
  }
}