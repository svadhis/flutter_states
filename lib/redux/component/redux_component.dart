import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/redux/state/redux_actions.dart';
import 'package:flutter_states/redux/state/redux_store.dart';
import 'package:redux/redux.dart';

class ReduxComponent extends StatelessWidget {
  const ReduxComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<ReduxStore, Store<ReduxStore>>(
      converter: (store) => store,
      builder: (context, model) {
        print(model.state);
        return Component(
          name: 'Redux',
          color: Colors.pink,
          counter: model.state.counter,
          duration: model.state.duration,
          callback: () {
            if (model.state.counter == 1) model.dispatch(ReduxActions.SetStart);
            if (model.state.counter == 99) model.dispatch(ReduxActions.SetEnd);
            model.dispatch(ReduxActions.Increment);
          } 
        );
      }
    );
  }
}