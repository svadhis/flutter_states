class ReduxStore {

  DateTime start;
  DateTime end;
  int counter = 0;

  int get duration => end != null ? end.difference(start).inMilliseconds : 0;

}