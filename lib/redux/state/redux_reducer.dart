import 'package:flutter_states/redux/state/redux_actions.dart';
import 'package:flutter_states/redux/state/redux_store.dart';

ReduxStore reduxReducer(ReduxStore store, dynamic action) {

  switch (action) {
    case ReduxActions.SetStart:
      store.start = DateTime.now();
      break;

    case ReduxActions.SetEnd:
      store.end = DateTime.now();
      break;

    case ReduxActions.Increment:
      store.counter++;
      if (store.counter > 100) store.counter = 1;
      break;
    
  }

  return store;

}