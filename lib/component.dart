import 'package:flutter/material.dart';

class Component extends StatelessWidget {
  const Component({@required this.name, @required this.color, @required this.counter, @required this.duration, @required this.callback, Key key}) : super(key: key);
  final String name;
  final ColorSwatch color;
  final int counter;
  final int duration;
  final Function callback;

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (counter > 0 && counter < 100) callback();
    });

    return Scaffold(
      appBar: AppBar(
        title: Text(name),
      ),
      body: Center(
        child: GestureDetector(
          onTap: callback,
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(5),
                  color: color[200]
                ),
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(left: 20 + counter.toDouble()),
                child: Text(name)
              ),
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Text(duration > 0 ? '${duration}ms' : '')
              )
            ],
          ),
        ),
      ),
    );
  }
}