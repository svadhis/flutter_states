import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/getx/state/obx_state.dart';
import 'package:get/get.dart';

class ObxComponent extends StatelessWidget {
  const ObxComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    ObxState model = Get.find<ObxState>();

    return Obx(() => Component(
      name: 'GetX - Obx',
      color: Colors.orange,
      counter: model.counter,
      duration: model.duration,
      callback: () => model.increment()
    ));
  }
}