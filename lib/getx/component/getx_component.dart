import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/getx/state/getx_state.dart';
import 'package:get/get.dart';

class GetxComponent extends StatelessWidget {
  const GetxComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetX<GetxState>(
      builder: (model) => Component(
        name: 'GetX',
        color: Colors.yellow,
        counter: model.counter,
        duration: model.duration,
        callback: () => model.increment()
      )
    );
  }
}