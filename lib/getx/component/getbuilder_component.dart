import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/getx/state/getbuilder_state.dart';
import 'package:get/get.dart';

class GetbuilderComponent extends StatelessWidget {
  const GetbuilderComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GetbuilderState>(
      builder: (model) => Component(
        name: 'GetX - Getbuilder',
        color: Colors.lightGreen,
        counter: model.counter,
        duration: model.duration,
        callback: () => model.increment()
      )
    );
  }
}