import 'package:flutter/material.dart';
import 'package:flutter_states/getx/component/getx_component.dart';
import 'package:flutter_states/getx/state/getx_state.dart';
import 'package:get/get.dart';

class GetxInjector extends StatelessWidget {
  const GetxInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Get.put<GetxState>(GetxState());

    return Container(
      child: GetxComponent(),
    );
  }
}