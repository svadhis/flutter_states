import 'package:flutter/material.dart';
import 'package:flutter_states/getx/component/getbuilder_component.dart';
import 'package:flutter_states/getx/state/getbuilder_state.dart';
import 'package:get/get.dart';

class GetbuilderInjector extends StatelessWidget {
  const GetbuilderInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Get.put<GetbuilderState>(GetbuilderState());

    return Container(
      child: GetbuilderComponent(),
    );
  }
}