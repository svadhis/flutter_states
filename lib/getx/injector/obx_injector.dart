import 'package:flutter/material.dart';
import 'package:flutter_states/getx/component/obx_component.dart';
import 'package:flutter_states/getx/state/obx_state.dart';
import 'package:get/get.dart';

class ObxInjector extends StatelessWidget {
  const ObxInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Get.put<ObxState>(ObxState());

    return Container(
      child: ObxComponent(),
    );
  }
}