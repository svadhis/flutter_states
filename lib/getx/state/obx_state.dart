import 'package:get/get.dart';

class ObxState extends GetxController {

  var _start = Rx<DateTime>();
  var _end = Rx<DateTime>();
  var _counter = Rx<int>(0);

  int get duration => _end.value != null ? _end.value.difference(_start.value).inMilliseconds : 0;
  
  int get counter => _counter.value;

  setStart() {
    _start.value = DateTime.now();
  }

  setEnd() {
    _end.value = DateTime.now();
  }

  increment() {
    if (_counter.value == 1) setStart();
    if (_counter.value == 99) setEnd();

    _counter.value++;

    if (_counter.value > 100) _counter.value = 1;
  }

}