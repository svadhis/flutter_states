import 'package:flutter/material.dart';

class StatefulWidgetComponent extends StatefulWidget {
  StatefulWidgetComponent({Key key}) : super(key: key);

  @override
  _StatefulWidgetComponentState createState() => _StatefulWidgetComponentState();
}

class _StatefulWidgetComponentState extends State<StatefulWidgetComponent> {
  DateTime _start;
  DateTime _end;
  int _counter = 0;

  int get duration => _end != null ? _end.difference(_start).inMilliseconds : 0;

  setStart() {
    setState(() => _start = DateTime.now());
  }

  setEnd() {
    setState(() => _end = DateTime.now());
  }

  increment() {
    if (_counter == 1) setStart();
    if (_counter == 99) setEnd();

    setState(() => _counter++);

    if (_counter > 100) setState(() => _counter = 1);
  }

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_counter > 0 && _counter < 100) increment();
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Stateful Widget'),
      ),
      body: Center(
        child: GestureDetector(
          onTap: () => increment(),
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.blue[200]
                ),
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(left: 20 + _counter.toDouble()),
                child: Text('Stateful Widget')
              ),
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Text(duration > 0 ? '${duration}ms' : '')
              )
            ],
          ),
        )
      )
    );
  }
}