import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/mobx/state/mobx_state.dart';
import 'package:provider/provider.dart';

class ProviderComponent extends StatelessWidget {
  const ProviderComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    MobxState model = Provider.of<MobxState>(context);

    return Observer(
      builder: (context) {
        return Component(
          name: 'Provider',
          color: Colors.green,
          counter: model.counter,
          duration: model.duration,
          callback: () => model.increment()
        );
      }
    );
  }
}