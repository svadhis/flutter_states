import 'package:flutter/material.dart';
import 'package:flutter_states/mobx/state/mobx_state.dart';
import 'package:provider/provider.dart';

class MobxInjector extends StatelessWidget {
  const MobxInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => MobxState(),
      child: MobxInjector()
    );
  }
}