import 'package:mobx/mobx.dart';

part 'mobx_state.g.dart';

class MobxState = MobxStateBase with _$MobxState;

abstract class MobxStateBase with Store {

  @observable
  DateTime _start;

  @observable
  DateTime _end;

  @observable
  int _counter = 0;

  @computed
  int get duration => _end != null ? _end.difference(_start).inMilliseconds : 0;
  
  @computed
  int get counter => _counter;

  @action
  setStart() {
    _start = DateTime.now();
  }

  @action
  setEnd() {
    _end = DateTime.now();
  }

  @action
  increment() {
    if (_counter == 1) setStart();
    if (_counter == 99) setEnd();

    _counter++;

    if (_counter > 100) _counter = 1;
  }

}