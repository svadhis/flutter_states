import 'package:flutter/material.dart';
import 'package:flutter_states/scoped_model/component/scoped_model_component.dart';
import 'package:flutter_states/scoped_model/state/scoped_model_state.dart';
import 'package:scoped_model/scoped_model.dart';

class ScopedModelInjector extends StatelessWidget {
  const ScopedModelInjector({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ScopedModelState>(
      model: ScopedModelState(),
      child: ScopedModelComponent()
    );
  }
}