import 'package:flutter/material.dart';
import 'package:flutter_states/component.dart';
import 'package:flutter_states/scoped_model/state/scoped_model_state.dart';
import 'package:scoped_model/scoped_model.dart';

class ScopedModelComponent extends StatelessWidget {
  const ScopedModelComponent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ScopedModelState>(
      builder: (context, child, model) {
        return Component(
          name: 'ScopedModel',
          color: Colors.red,
          counter: model.counter,
          duration: model.duration,
          callback: () => model.increment()
        );
      }
    );
  }
}