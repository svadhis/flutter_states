import 'package:scoped_model/scoped_model.dart';

class ScopedModelState extends Model {

  DateTime _start;
  DateTime _end;
  int _counter = 0;

  int get duration => _end != null ? _end.difference(_start).inMilliseconds : 0;
  
  int get counter => _counter;

  setStart() {
    _start = DateTime.now();
  }

  setEnd() {
    _end = DateTime.now();
  }

  increment() {
    if (_counter == 1) setStart();
    if (_counter == 99) setEnd();

    _counter++;

    if (_counter > 100) _counter = 1;

    notifyListeners();
  }

}